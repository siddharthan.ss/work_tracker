from django.apps import apps
from django.contrib import admin

from .models import *

for model in apps.get_app_config('core').models.values():
        # admin.site.register(model)
        pass

admin.site.register(SubTask)
admin.site.register(Task)
admin.site.register(TaskUser)
admin.site.register(Project)