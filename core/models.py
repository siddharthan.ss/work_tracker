from django.contrib.auth.models import User
from django.db import models


# TODO add your suggested models here


class SubTask(models.Model):
    score = models.IntegerField()
    ref = models.ManyToManyField('self', blank=True)
    Branch = models.BooleanField()
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Task(models.Model):
    score = models.IntegerField()
    ref = models.ManyToManyField(SubTask, blank=True)
    Branch = models.BooleanField()
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Project(models.Model):
    ProjectName = models.CharField(max_length=100)
    StartDate = models.DateField()
    TaskReference = models.ManyToManyField(Task)
    employees = models.ManyToManyField(User)

    def __str__(self):
        return self.ProjectName


class TaskUser(models.Model):
    task = models.ForeignKey(Task)
    subTask = models.ForeignKey(SubTask, blank=True, null=True)
    user = models.ForeignKey(User)
    status = models.BooleanField(default=False)
