from rest_framework import serializers
from rest_framework.relations import HyperlinkedIdentityField

from core.models import *

# TODO link serializers
programme_detail_url = HyperlinkedIdentityField(
    view_name='api_accounts:programmeDetail',
    lookup_field='pk',
)


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'


class SubTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubTask
        fields = '__all__'


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = '__all__'


class TaskUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskUser
        fields = '__all__'
