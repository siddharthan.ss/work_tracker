from django.conf.urls import url, include
from django.contrib import admin

from core.views.project import *
from core.views.subtask import *
from core.views.task import *
from core.views.task_user import *

'''
core/
'''

admin.autodiscover()

urlpatterns = [
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),

    url(r'^sub-task/$', SubTaskList.as_view(), name='sub-task-list'),
    url(r'^sub-task/(?P<pk>[\w-]+)/$', SubTaskDetailed.as_view(), name='sub-task-detail'),
    url(r'^sub-task/create/$', SubTaskCreate.as_view(), name='sub-task-create'),
    url(r'^sub-task/(?P<pk>[\w-]+)/edit/$', SubTaskUpdate.as_view(), name='sub-task-edit'),
    url(r'^sub-task/(?P<pk>[\w-]+)/delete/$', SubTaskDelete.as_view(), name='sub-task-delete'),

    url(r'^task/$', TaskList.as_view(), name='task-list'),
    url(r'^task/(?P<pk>[\w-]+)/$', TaskDetailed.as_view(), name='task-detail'),
    url(r'^task/create/$', TaskCreate.as_view(), name='task-create'),
    url(r'^task/(?P<pk>[\w-]+)/edit/$', TaskUpdate.as_view(), name='task-edit'),
    url(r'^task/(?P<pk>[\w-]+)/delete/$', TaskDelete.as_view(), name='task-delete'),

    url(r'^project/$', ProjectList.as_view(), name='project_list'),
    url(r'^project/(?P<pk>[\w-]+)/$', ProjectDetailed.as_view(), name='project-detail'),
    url(r'^project/create/$', ProjectCreate.as_view(), name='project-create'),
    url(r'^project/(?P<pk>[\w-]+)/edit/$', ProjectUpdate.as_view(), name='project-update'),
    url(r'^project/(?P<pk>[\w-]+)/delete/$', ProjectDelete.as_view(), name='project-delete'),

    url(r'^task-user-user/$', ProjectList.as_view(), name='task-user-user'),
    url(r'^task-user/$', TaskUserList.as_view(), name='task-user-list'),
    url(r'^task-user/(?P<pk>[\w-]+)/$', TaskUserDetailed.as_view(), name='task-user-detail'),
    url(r'^task-user/create/$', TaskUserCreate.as_view(), name='task-user-create'),
    url(r'^task-user/(?P<pk>[\w-]+)/edit/$', TaskUserUpdate.as_view(), name='task-user-edit'),
    url(r'^task-user/(?P<pk>[\w-]+)/delete/$', TaskUserDelete.as_view(), name='task-user-delete'),

]
