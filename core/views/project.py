from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from core.serializers import *

'''
core/project/
'''


class ProjectList(ListAPIView):
    serializer_class = ProjectSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = Project.objects.all()
        search_type = 'icontains'
        c = []
        for f in Project._meta.get_fields():
            a = str(f)
            if 'core.Project' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)

            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     )
        return queryset_list


class ProjectCreate(CreateAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class ProjectDetailed(RetrieveAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class ProjectDetailedLink(RetrieveAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class ProjectUpdate(RetrieveUpdateAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class ProjectDelete(DestroyAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
