from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from core.serializers import *

'''
core/task/user-task/
'''


class TaskUserList(ListAPIView):
    serializer_class = TaskUserSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    permission_classes = [AllowAny]
    search_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        queryset_list = TaskUser.objects.all()
        search_type = 'icontains'
        c = []
        for f in TaskUser._meta.get_fields():
            a = str(f)
            if 'core.TaskUser' in a:
                c.append(str(f.name))
        print(c)
        for field in c:
            print(field)
            query = self.request.GET.get(field)
            filter = field + '__' + search_type
            print(query)
            if query:
                queryset_list = queryset_list.filter(**{filter: query}
                                                     )
        return queryset_list


class TaskUserCreate(CreateAPIView):
    queryset = TaskUser.objects.all()
    serializer_class = TaskUserSerializer
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        serializer.save()


class TaskUserDetailed(RetrieveAPIView):
    queryset = TaskUser.objects.all()
    serializer_class = TaskUserSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class TaskUserDetailedLink(RetrieveAPIView):
    queryset = TaskUser.objects.all()
    serializer_class = TaskUserSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class TaskUserUpdate(RetrieveUpdateAPIView):
    queryset = TaskUser.objects.all()
    serializer_class = TaskUserSerializer
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        serializer.save()


class TaskUserDelete(DestroyAPIView):
    queryset = TaskUser.objects.all()
    serializer_class = TaskUserSerializer
    lookup_field = 'pk'
    permission_classes = [IsAdminUser, IsAuthenticated]
