from django.conf.urls import url
from django.contrib import admin

from .views import home, register

admin.autodiscover()

urlpatterns = [
    url(r'^register$', register, name='register'),
    url(r'^$', home, name='home'),
]
