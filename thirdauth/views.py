# Create your views here.

from django.contrib.auth.decorators import login_required
from django.shortcuts import render


@login_required
def home(request):
    return render(request, 'home.html')


def register(request):
    return render(request, 'register.html')
